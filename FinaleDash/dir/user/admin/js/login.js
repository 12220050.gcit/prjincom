const emailInput = document.getElementById('email');
const passwordInput = document.getElementById('password');
const submitBtn = document.getElementById('submit-btn');
const errorMessage = document.getElementById('error-message');

submitBtn.addEventListener('click', (e) => {
  e.preventDefault();

  const email = emailInput.value;
  const password = passwordInput.value;

  if (email === 'admin@example.com' && password === 'admin123') {
    window.location.href = 'dash.html'; // Replace with the URL of your dashboard page
  } else {
    errorMessage.textContent = 'Invalid email or password';
  }
});
