window.onload = function() {
    fetch('/items')
      .then(response => response.text())
      .then(data => showStudents(data));
      
  }
  
  function showStudents(data) {
    const students = JSON.parse(data);
    console.log(students);
    students.forEach(stud => {
      newRow(stud);
    });
  }
  
  var x = 1
  function newRow(student) {
    console.log(student);
  
    var name = student.name;
    var content = student.content;
    var quantity = student.quantity;
    var category = student.catname;
    var specification = student.specification;
  
    const section = document.querySelector('section');
    const container1 = document.createElement('div');
    container1.classList.add('container1');
    const title = document.createElement('h3');
    title.classList.add('title');
    const productsContainer = document.createElement('div');
    productsContainer.classList.add('products-container');
    const product = document.createElement('div');
    product.classList.add('product');
    product.setAttribute('data-name', 'p-1');
    const image = document.createElement('img');
    image.src = content;
    image.alt = '';
    const heading = document.createElement('h3');
    heading.textContent = name;
  
    product.appendChild(image);
    product.appendChild(heading);
    productsContainer.appendChild(product);
    container1.appendChild(title);
    container1.appendChild(productsContainer);
  
    const productsPreview = document.createElement('div');
    productsPreview.classList.add('products-preview');
    const preview = document.createElement('div');
    preview.classList.add('preview');
    preview.setAttribute('data-target', 'p-1');
    const closeIcon = document.createElement('i');
    closeIcon.classList.add('fa', 'fa-times');
    const previewHeading = document.createElement('h3');
    previewHeading.textContent = name;
    const previewText = document.createElement('p');
    previewText.style.marginTop = '10px';
    previewText.textContent = specification;
    const nameInput = document.createElement('input');
    nameInput.setAttribute('type', 'ItemName');
    nameInput.setAttribute('id', 'quantity');
    nameInput.setAttribute('name', 'ItemName');
    nameInput.setAttribute('placeholder', 'Name');
    const quantityInput = document.createElement('input');
    quantityInput.setAttribute('type', 'ItemName');
    quantityInput.setAttribute('id', 'quantity');
    quantityInput.setAttribute('name', 'ItemName');
    quantityInput.setAttribute('placeholder', 'Quantity');
    const nameLabel = document.createElement('label');
    nameLabel.setAttribute('for', 'username');
    nameLabel.textContent = 'Name';
    const quantityLabel = document.createElement('label');
    quantityLabel.setAttribute('for', 'username');
    quantityLabel.textContent = quantity;
    const requestButton = document.createElement('button');
    requestButton.classList.add('request');
    requestButton.textContent = 'Request';
  
    preview.appendChild(closeIcon);
    preview.appendChild(previewHeading);
    preview.appendChild(previewText);
    preview.appendChild(nameLabel);
    preview.appendChild(nameInput);
    preview.appendChild(quantityLabel);
    preview.appendChild(quantityInput);
    preview.appendChild(requestButton);
  
    productsPreview.appendChild(preview);
  
    section.appendChild(container1);
    section.appendChild(productsPreview);
    x = 2
  }
  
  if (x == 2){
    let previewContainer = document.querySelector(".products-preview");
    let previewBox = previewContainer.querySelectorAll(".preview");
  
    document.querySelectorAll(".products-container .product").forEach((product) => {
      product.onclick = () => {
        previewContainer.style.display = "flex";
        let name = product.getAttribute("data-name");
        previewBox.forEach((preview) => {
          let target = preview.getAttribute("data-target");
          if (name == target) {
            preview.classList.add("active");
          }
        });
      };
    });
  
    previewBox.forEach((close) => {
      close.querySelector(".fa-times").onclick = () => {
        close.classList.remove("active");
        previewContainer.style.display = "none";
      };
    });
  }
 