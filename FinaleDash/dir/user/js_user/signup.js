const useridEl = document.querySelector("#userid");
const usernameEl = document.querySelector("#username");
const emailEl = document.querySelector("#email");
const passwordEl = document.querySelector("#password");
const confirmPasswordEl = document.querySelector("#confirm-password");
const form = document.querySelector("#signup");

form.addEventListener("submit", function (e) {
  e.preventDefault();

    let isUseridValid = checkUserid(),
    isUsernameValid = checkUsername(),
    isEmailValid = checkEmail(),
    isPasswordValid = checkPassword(),
    isConfirmPasswordValid = checkConfirmPassword();
  let isFormValid =
    isUseridValid &&
    isUsernameValid &&
    isEmailValid &&
    isPasswordValid &&
    isConfirmPasswordValid;
  if (isFormValid) {
    // window.location.href = "login.html"
    var data = {
      id: document.getElementById("userid").value,
      name: document.getElementById("username").value,
      email: document.getElementById("email").value,
      password: document.getElementById("password").value,
    }
    // ids = document.getElementById("userid").value
    // console.log("working or not",ids)
    console.log(data)
    fetch("/user",{
    method: "POST",
    body: JSON.stringify(data),
    headers: {"Content-type": "application/json; charset=UTF-8"}
  }).then(response => {
    console.log("Response status:", response.status);
    if (response.status == 201) {
      window.location.href = "login.html"
      console.log("here it is")
    }else{
       throw new Error(response)
    }
  })
  }
});
  

function checkUsername() {
  let valid = false;
  const min = 3,
    max = 25;
  const username = usernameEl.value.trim();
  if (!isRequired(username)) {
    showError(usernameEl, "Username cannot be blank.");
  } else if (!isBetween(username.length, min, max)) {
    showError(
      usernameEl,
      `Username must be between ${min} and ${max} characters.`
    );
  } else {
    showSuccess(usernameEl);
    valid = true;
  }
  return valid;
}

const isUsernameValid = (username) => {
  const re = /^[A-Za-z]+$/;
  return re.test(username);
};

function checkUserid() {
  let valid = false;
  const min = 10,
    max = 25;
  const userid = useridEl.value.trim();
  if (!isRequired(userid)) {
    showError(useridEl, "Staff ID cannot be blank.");
  } 
  else if (!isBetween(userid.length, min, max)) {
    showError(
      useridEl,
      `Staff ID must be between ${min} and ${max} characters.`
    );
  } 
   else if (!isUseridValid(userid)) {
    showError(useridEl, "Invalid staff ID. Must start with RUB");
  } else {
    showSuccess(useridEl);
    valid = true;
  }
  return valid;
}
//Regular expression to validate the student name pattern
const isUseridValid = (userid) => {
  const re =/^rub\d{7}$/;

    // /^([A-Z]{1}[a-zA-Z]+)(\s[A-Z]{1}[a-zA-Z]+)?(\s[A-Z]{1}[a-zA-Z]+)?$/;
  return re.test(userid);
};




let isRequired = (value) => (value === "" ? false : true);

const isBetween = (length, min, max) =>
  length < min || length > max ? false : true;

let showError = (input, message) => {
  let formField = input.parentElement;
  formField.classList.remove("success");
  formField.classList.add("error");

  const error = formField.querySelector("small");
  error.textContent = message;
};

const showSuccess = (input) => {
  const formField = input.parentElement;
  formField.classList.remove("error");
  formField.classList.add("success");

  formField.querySelector("small").textContent = "";
};



function checkEmail() {
  let valid = false;
  const email = emailEl.value.trim();
  if (!isRequired(email)) {
    showError(emailEl, " Email cannot be blank.");
  } else if (!isEmailValid(email)) {
    showError(emailEl, `Email is not valid. Should be like example@gcit.rub.edu.bt`);
  } else {
    showSuccess(emailEl);
    valid = true;
  }
  return valid;
}
const isEmailValid = (email) => {
  const re =/^[A-Za-z0-9]+(?:\w+\.)?gcit@rub\.edu\.bt$/
    return re.test(email);
};


function checkPassword() {
  let valid = false;
  const min = 3,
    max = 25;
  const password = passwordEl.value.trim();
  if (!isRequired(password)) {
    showError(passwordEl, "You have to set a password");
  } else if (!isPasswordValid(password)) {
    showError(
      passwordEl,
      "Password must be at least 8 characters that include at least 1 lowercase character, 1 uppercase characters, 1 number and 1 special character "
    );
  } else {
    showSuccess(passwordEl);
    valid = true;
  }
  return true;
}
const isPasswordValid = (password) => {
  const re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
  return re.test(password);
};

function checkConfirmPassword() {
  let valid = false;
  const passwordCheck = confirmPasswordEl.value.trim();
  if (!isRequired(passwordCheck)) {
    showError(confirmPasswordEl, "You will have to confirm to proceed");
  } else if (!passwordMatch()) {
    showError(confirmPasswordEl, "Password is not matching");
  } else {
    showSuccess(confirmPasswordEl);
    valid = true;
  }
  return valid;
}
const passwordMatch = () => {
  if (passwordEl.value == confirmPasswordEl.value) {
    return true;
  }
};
