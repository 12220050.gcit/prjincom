package model

import (
	"context"
	mongosh "finalDash/datastore"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type User struct{
	Id string 	//`json:"id"`
	Name string //`json:"name"` 
	Email string //`json:"email"`
	Password string //`json:"password"`
}
var collections = mongosh.Client.Database("store").Collection("account")
func (u *User) AddUser(pass []byte) error {
	indexModel := mongo.IndexModel{
        Keys: bson.M{
            "Id": 1,
        },
        Options: options.Index().SetUnique(true),
    }
    _, Inderr := collections.Indexes().CreateOne(context.Background(), indexModel)
    if Inderr != nil {
        return Inderr
    }
	_, err := collections.InsertOne(context.Background(),bson.D{
		{Key:"Id",Value:u.Id},
		{Key: "Name",Value:u.Name},
		{Key:"Email",Value:u.Email},
		{Key: "Password",Value:pass},
	})
	return err

}

func (u *User) Check() (*mongo.Cursor, error){
	filter := bson.M{
		"Id":u.Id,
	}
	cur, err := collections.Find(context.Background(),filter)
	return cur, err
}